Tag: debian-watch-file-unknown-version
Severity: warning
Check: debian/watch
See-Also: uscan(1)
Explanation: The <code>version=</code> line in the <code>debian/watch</code> file in this
 package declares an unknown version. The currently known watch file
 versions are 2, 3 and 4
