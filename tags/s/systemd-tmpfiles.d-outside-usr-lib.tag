Tag: systemd-tmpfiles.d-outside-usr-lib
Severity: error
Check: systemd
Explanation: The package ships a systemd tmpfiles.d(5) conf file outside
 <code>/usr/lib/tmpfiles.d/</code>
