Tag: tar-errors-from-data
Severity: error
Check: deb-format
Explanation: tar produced an error while listing the contents of the data
 member of this package. This probably means there's something broken or
 at least strange about the way the package was constructed.
